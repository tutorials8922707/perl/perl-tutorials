#!/usr/bin/env perl

%tennis = (1 => 'Novak', 2 => 'Roger', 3 => 'Andy', 4 => 'Kei');

@my_arr=keys %tennis;
$size=@my_arr;
print "The size of the hash initially is: $size\n";

$tennis{5}='Berdych';

@my_arr=keys %tennis;
$size=@my_arr;
print "The size of the hash after addition is: $size\n";

delete $tennis{5};

@my_arr=keys %tennis;
$size=@my_arr;
print "The size of the hash after addition is: $size\n";
