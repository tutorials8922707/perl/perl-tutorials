#!/usr/bin/env perl

@string_arr=('One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Severn');
print "Original string list: @string_arr\n";

@string_arr=sort(@string_arr);
print "Sorted string list: @string_arr\n";

@arr1=(1..5);
@arr2=(6..10);

@final_arr=(@arr1, @arr2);
print "Merged array: @final_arr\n";

# my
@num_arr=(-1, 12, 0, 8, -500, 1000, -100, -0.5, 4);
@num_sort=sort(@num_arr);
print "UnSorted numbers: @num_arr\n";
print "Sorted numbers: @num_sort\n";
