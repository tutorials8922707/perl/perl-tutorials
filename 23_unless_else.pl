#!/usr/bin/env perl

$x = 10;

unless ($x % 2 == 0) {
	print "x is odd\n";
} else {
	print "x is even\n";
}
